from django.http import JsonResponse, HttpResponse
from django.core.mail import send_mail
from django.conf import settings



def nmbItem(request):
    url = request.GET.get('url')
    change = request.GET.get('change')

    totalPrice = request.session['cartInfo']['totalPrice']
    data = {}

    for i in range(len(request.session['cart'])):

        if request.session['cart'][i]['url'] == url:
            nmb = request.session['cart'][i]['nmb']
            pricePerItem = int(request.session['cart'][i]['price'])

            if change == 'plus' and nmb != 10:
                nmb += 1
                totalPrice += pricePerItem
            elif change == 'minus' and nmb != 1:
                nmb -= 1
                totalPrice -= pricePerItem

            data.update({
                'nmb': nmb,
                'totalPrice': totalPrice
            })

            request.session['cart'][i].update({
                'nmb': nmb
            })

            request.session['cartInfo'].update({
                'totalPrice': totalPrice
            })

    return JsonResponse(data)


def checkout(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        tel = request.POST.get('tel')
        email = request.POST.get('email')
        message = request.POST.get('message')

        items = request.session['cart']
        cartInfo = request.session['cartInfo']

        if (name == '') or (len(name) < 2):
            return HttpResponse('name_error', content_type='text/html')

        try:
            if not '.' in email.split('@')[1]:
                return HttpResponse('email_error', content_type='text/html')

        except:
            return HttpResponse('email_error', content_type='text/html')

        br = '\n'
        data = 'Информация о заказчике:' + br + name + br + tel + br + email + br + message + br*2

        if items != []:
            data += 'Информация о заказе:' + br
            for item in items:
                data += 'Название:  ' + item['name'] + br
                data += 'Цена:  ' + str(item['price']) + ' руб.' + br
                data += 'Кол-во:  ' + str(item['nmb']) + br*2

            data += 'Итого:  ' + str(cartInfo['totalPrice']) + ' руб.'

        subject = 'Заказ на покупку'

        del request.session['cart']
        del request.session['cartInfo']
        send_mail(subject, data, settings.EMAIL_HOST_USER, ['qreditry@gmail.com'], fail_silently=False)

        return HttpResponse('ok', content_type='text/html')
