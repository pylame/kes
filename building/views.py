from django.shortcuts import render, get_object_or_404
from .models import itemBuilding

def home(request):
    itemsBuilding = itemBuilding.objects.all()

    return render(request, 'building/building.html')
# , {'itemsBuilding': itemsBuilding}


def itemPage(request, slug):
    itemModel = get_object_or_404(itemBuilding, slug=slug)

    return render(request, 'building/itemBuilding.html', {'itemBuilding': itemModel})