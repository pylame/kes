from django.db import models


class itemBuilding(models.Model):
    name = models.CharField(blank=True, null=True, max_length=100, verbose_name='Название')
    nameInMenu = models.CharField(blank=True, null=True, max_length=100, verbose_name='Название в меню')
    slug = models.SlugField(blank=True, null=True, verbose_name='Название в url')
    descr = models.TextField(blank=True, null=True, max_length=5000, verbose_name='Небольшое описание')
    url = models.CharField(blank=True, null=True, max_length=100, editable=False)
    title = models.CharField(blank=True, null=True, max_length=100, verbose_name='Тег title')
    text = models.TextField(blank=True, null=True, max_length=5000, verbose_name='Текст страницы')
    features1 = models.TextField(blank=True, null=True, max_length=150, verbose_name='Преимущество 1')
    features2 = models.TextField(blank=True, null=True, max_length=150, verbose_name='Преимущество 2')
    features3 = models.TextField(blank=True, null=True, max_length=150, verbose_name='Преимущество 3')

    def save(self, *args, **kwargs):
        super(itemBuilding, self).save(*args, **kwargs)
        self.url = '/building/' + self.slug
        super(itemBuilding, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория строительства'
        verbose_name_plural = 'Категории строительства'
