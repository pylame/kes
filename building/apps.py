from django.apps import AppConfig

class BuildingAppConfig(AppConfig):
    name = "building"
    verbose_name = "Категории строительства"
