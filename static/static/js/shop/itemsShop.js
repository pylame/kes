$(function () {
  //--------------------------ARROW-------------------------------
  var filterHeader = $('.filter__header');
  var filterFieldset = $('.filter__fieldset');
  var filterArrow = $('.filter__arrow');
  var filterFieldsets = $('.filter__fieldsets');
  var deg = 180;

  filterHeader.click(function () {
    filterFieldsets.slideToggle(300);
    filterArrow.css('transform', 'rotate(' + deg + 'deg)');
    deg += 180;
  });

  //---------------------------FILTER-SEARCH-------------------------------
  var filterForm = $('#filter');
  var searchForm = $('#search');
  var searchInput = $('.search__result');
  var paginatorButton = $('.paginator');

  searchAjax('page=1&search=', true);

  filterForm.submit(function (e) {
    e.preventDefault();
    searchInput.val('');
    paginatorButton.val(2);
    if (!filterFieldset.hasClass('hidden')) {
      filterHeader.click();
    }
    var remove = true;
    var data = getDataFilter() + '&page=1';
    filterAjax(data, remove);
  });

  searchForm.submit(function (e) {
    e.preventDefault();
    paginatorButton.val(2);
    var remove = true;
    var data = searchForm.serialize();
    data += '&page=1';
    searchAjax(data, remove);
  });


  //----------------------------CART-----------------------------
  var resultContainer = $('.result-container');
  var cartTotal = $('.cart__item--total');
  resultContainer.on('submit', '.product__buy', function (e) {
    e.preventDefault();
    var url = $(this).data('slug');
    var btn = $(this).find('button');

    //ajax
    var data = {
      'url': url
    };

    addCartAjax(data, btn, cartTotal);
  });

  //--------------------------PAGINATOR-------------------------------
  paginatorButton.click(function () {
    paginatorButton.prop('disabled', true);
    var remove = false;
    var numPage = parseInt(paginatorButton.val());
    var data = 'page=' + numPage + '&';
    if (searchInput.val() !== '') {
      data += searchForm.serialize();
      searchAjax(data, remove);
    } else {
      data += getDataFilter();
      filterAjax(data, remove);
    }
    paginatorButton.val(numPage + 1);
  })

});

//----------------------------------------------------------AJAX-FUNCTIONS---------------------------------------------------------------------------

function getItems(data, remove) {
  var items = data.items;
  var paginatorButton = $('.paginator');
  var container = $('.result-container');
  if (remove) {
    container.empty();
  }
  if (!data.hasNext) {
    paginatorButton.css('display', 'none');
  } else {
    paginatorButton.prop('disabled', false);
    paginatorButton.css('display', 'block');
  }
  if (data.pages === 0) {
    container.append('<p class="result-container__alert">Товаров не найдено</p>');
  } else {
    items.forEach(function (item) {
      var product = '<a href="' + item.slug + '" class="product">'; // fixme: start kostyl ebanniy
      product += '<h2 class="product__title">' + item.name + '</h2>' +
      '<div class="product__info"><div class="product__img"></div><div class="product__wrapper">';
      if (item.power) {
        product += '<span class="product__power">' + 'Мощность: ' + item.power + ' кВт' + '</span>';
      }
      if (item.minAmount && item.maxAmount) {
        product += '<span class="product__amount">' + 'Объем: ' + item.minAmount + 'м<sup>3</sup> - ' + item.maxAmount + 'м<sup>3</sup>' + '</span>';
      }
      if (item.price) {
        product += '<span class="product__price">' + 'Цена: ' + item.price + '</span>';
      }
      product += '<form action="cart_add=" method="get" class="product__buy" data-slug="' + item.slug + '" data-name="' + item.name + '" data-price="' + item.price + '">' +
      '<button type="submit" class="product__add-cart btn">добавить в корзину</button></form></div></div></a>'; // end kostyl
      container.append(product);
    });

  }
}

function filterAjax(data, remove) {
  var filterForm = $('#filter');

  $.ajax({
    type: 'GET',
    url: filterForm.attr('action'),
    data: data,
    dataType: 'json',
    success: function (data) {
      getItems(data, remove);
    }
  });
}

function searchAjax(data, remove) {
  var searchForm = $('#search');
  $.ajax({
    type: 'GET',
    url: searchForm.attr('action'),
    data: data,
    dataType: 'json',
    success: function (data) {
      getItems(data, remove);
    }
  });
}

function getDataFilter() {
  var filterForm = $('#filter');
  var data = filterForm.serializeArray();
  for (var i = 0; i < data.length; i++) {
    if (data[i].value === '') {
      data.splice(i, 1);
      i--;
    }
  }

  data = $.param(data);

  return data
}