$(function () {
  var itemPageBuy = $('.item-page__form-buy');
  var cartTotal = $('.cart__item--total');

  itemPageBuy.submit(function (e) {
    e.preventDefault();
    var url = $(this).data('slug');
    var btn = $(this).find('button');

    var data = {
      'url': url
    };
    addCartAjax(data, btn, cartTotal);
  });

  var resultContainer = $('.cognate-items');
  resultContainer.on('submit', '.product__buy', function (e) {
    e.preventDefault();
    var url = $(this).data('slug');
    var btn = $(this).find('button');

    var data = {
      'url': url
    };

    addCartAjax(data, btn, cartTotal);
  });
});
