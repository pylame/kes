$(function () {

  //-----------------------------------CART--------------------------------------
  var navCart = $('.main-nav__cart');

  navCart.empty();
  navCart.addClass('main-nav__cart--main-page');

  var cart = $('.main-cart');
  cart.on('click', '.main-cart__remove-btn', function () {
    removeCartAjax(this, '.main-cart__item', true);
  });


  //--------------------------------PLUS-MINUS-----------------------------------

  cart.on('click', '.main-cart__number', function () {
    var thisItem = $(this).closest('.main-cart__item');
    var nmb = thisItem.find('.main-cart__nmb');
    var url = thisItem.find('.main-cart__link').attr('href');
    var change = $(this).hasClass('main-cart__number--plus');
    var totalPrice = $('.checkout__total-price');

    if (change) {
      change = 'plus'
    } else {
      change = 'minus'
    }
    $.ajax({
      type: 'GET',
      url: 'nmbItem=',
      data: {
        'url': url,
        'change': change
      },
      cache: false,
      success: function (data) {
        nmb.val(data.nmb);
        totalPrice.text('Итого: ' + data.totalPrice + ' руб.')
      }
    });
  });

  //--------------------------------CHECKOUT-----------------------------------
  var main = $('main');
  var order = $('#order');
  var orderBtn = $('.order__submit');
  var orderName = $('#orderName');
  var orderEmail = $('#orderEmail');

  order.submit(function (e) {
    orderBtn.prop('disabled', true);
    e.preventDefault();
    var data = $(this).serialize();
    $.ajax({
      type: 'POST',
      url: 'checkout',
      data: data,
      cache: false,
      success: function (data) {
        if (data === 'ok') {
          main.empty();
          main.append('<div class="checkout"><div class="checkout__header text-header"><h1 class="checkout__h text-h">Ваша корзина:</h1></div></div>');
          main.append('<div class="result-container"><p class="result-container__alert">Спасибо за заказ, в ближайшее время мы с вами свяжемся.</p></div>');
        } else if (data === 'name_error') {
          orderName.val('');
          orderName.attr('placeholder', 'Введите корректное имя');
          orderBtn.prop('disabled', false);
        } else if (data === 'email_error') {
          orderEmail.val('');
          orderEmail.attr('placeholder', 'Введите корректный email');
          orderBtn.prop('disabled', false);
        }
      }
    });
  })


});
