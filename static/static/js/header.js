$(function () {
  //---------------------POPUP-REQUEST------------------------------
  var popup = $('.popup-request');
  var openPopup = false;

  $('.info__btn, .request__btn').click(function () {
    openPopup = true;
    openModal(popup, false);
  });

  $('.popup-request__close').click(function () {
    openPopup = false;
    closeModal(popup, false);
  });

  //---------------------MAIN-NAV------------------------------

  var navMainWrapper = $('.main-nav__wrapper');
  var navMainBtn = $('.main-nav__toggle');
  var openMainNav = false;

  navMainBtn.click(function () {
    openMainNav = !openMainNav;
    if (openMainNav) {
      openModal(navMainWrapper, navMainBtn);
    } else {
      closeModal(navMainWrapper, navMainBtn);
    }
  });



  //---------------------CART------------------------------
  var cartBtn = $('.main-nav__cart');
  var cart = $('.cart');
  var openCart = false;

  cartBtn.click(function () {
    openCart = !openCart;
    if (openCart) {
      openModal(cart, cartBtn);
    } else {
      closeModal(cart, cartBtn);
    }
  });

  cart.on('click', '.cart__remove', function () {
    removeCartAjax(this, '.cart__item', false)
  });


  //---------------------OVERLAY------------------------------
  $('.overlay').click(function () {
    if (openPopup) {
      openPopup = false;
      closeModal(popup, false);
    } else if (openMainNav) {
      openMainNav = false;
      closeModal(navMainWrapper, navMainBtn);
    } else if (openCart) {
      openCart = false;
      closeModal(cart, cartBtn);
    }
  });


  //---------------------FEEDBACK-FORM------------------------------
  var form = $('#feedbackForm');
  var name = $('#feedbackName');
  var email = $('#feedbackEmail');
  var thanks = $('.popup-request__thanks');
  var submit = $('#feedbackSubmit');

  $(form).submit(function (e) {
    submit.prop('disabled', true);
    e.preventDefault();
    var data = $(this).serialize();
    $.ajax({
      type: 'POST',
      url: 'request_email',
      data: data,
      cache: false,
      success: function (data) {
        if (data === 'ok') {
          form.remove();
          thanks.removeClass('hidden');
        }
        else if (data === 'name_error') {
          name.val('');
          name.attr('placeholder', 'Введите корректное имя');
          submit.prop('disabled', false);
        }
        else if (data === 'email_error') {
          email.val('');
          email.attr('placeholder', 'Введите корректный email');
          submit.prop('disabled', false);
        }
      }
    });
  });
});

//----------------------------CART-FUNKS-----------------------------------

function addCartAjax(data, btn, cartTotal) {
  $.ajax({
    url: 'cart_add=',
    type: 'GET',
    data: data,
    cache: true,
    success: function (data) {
      if (data === 'item_is_available') {
        btn.prop('disabled', true);
        btn.text('товар уже в корзине');
        setTimeout(function () {
          btn.prop('disabled', false);
          btn.text('добавить в корзину');
        }, 2000)
      } else if (data.cart) {
        btn.prop('disabled', true);
        btn.text('товар добавлен!');
        setTimeout(function () {
          btn.prop('disabled', false);
          btn.text('добавить в корзину');
        }, 2000);

        var products = $('.cart__item');
        for (var i = 0; i < products.length - 1; i++) {
          products[i].remove();
        }

        var items = data.cart;
        items.forEach(function (item, i) {
          var product = '<li class="cart__item"><a href="' + item.url + '" class="cart__link">' + // fixme: start kostyl ebanniy
            '<span class="cart__title-item">' + (i + 1) + ':</span>' +
            '<span class="cart__name">' + item.name + '</span><br>' +
            '<span class="cart__price">' + item.price + ' руб.</span>' +
            '</a><button type="button" class="cart__remove" data-url="' + item.url + '"></button></li>'; // end kostyl

          cartTotal.before(product);
        });

        conversionHeaderCart(data);
      }
    },
    error: function () {
      console.log('error')
    }
  });
}

function removeCartAjax(removeBtn, itemInCart, checkoutPage) {
  var url = $(removeBtn).data('url');
  var item = $(removeBtn).closest(itemInCart);
  var data = {
    'url': url
  };
  $.ajax({
    url: 'cart_remove=',
    type: 'GET',
    data: data,
    cache: true,
    success: function (data) {
      if (data === 'not_found') {
        console.log('not_found')
      } else {
        item.remove();
        if (checkoutPage) {
          conversionCheckoutCart(data);
        } else {
          conversionHeaderCart(data);
        }
      }
    }
  });
}

function conversionHeaderCart(data) {
  var cart = $('.main-nav__cart');
  var totalItems = $('.cart__total-item');
  var totalPrice = $('.cart__total-price');

  totalItems.text(data.totalItems);
  totalPrice.text(data.totalPrice + ' руб.');
  if (data.totalItems === 0) {
    cart.empty();
  } else {
    cart.html('<span class="main-nav__num-cart">' + data.totalItems + '</span>')
  }
}

function conversionCheckoutCart(data) {
  if (data.totalItems === 0) {
    var main = $('main');
    main.empty();
    main.append('<div class="checkout"><div class="checkout__header text-header"><h1 class="checkout__h text-h">Ваша корзина:</h1></div></div>');
    main.append('<div class="result-container"><p class="result-container__alert">Корзина пуста</p></div>');
  } else {
    var totalItems = $('.checkout__len-items');
    var totalPrice = $('.checkout__total-price');

    totalItems.text('Товаров в корзине: ' + data.totalItems);
    totalPrice.text('Итого: ' + data.totalPrice + ' руб.');
  }
}

function openModal(element, btn) {
  var overlay = $('.overlay');

  if (btn !== false) {
    btn.css('z-index', 10);
    element.css('z-index', 10);
    if (btn.hasClass('main-nav__toggle')) {
      btn.addClass('main-nav__toggle--opened');
    }
  }

  element.fadeIn(300);
  overlay.fadeIn(300);
}

function closeModal(element, btn) {
  var overlay = $('.overlay');

  if (btn !== false) {
    btn.removeAttr('style');
    if (btn.hasClass('main-nav__toggle')) {
      btn.removeClass('main-nav__toggle--opened');
    }
  }

  element.fadeOut(300, function () {
    element.removeAttr('style');
  });
  overlay.fadeOut(300);
}
