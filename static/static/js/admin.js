$(function () {
  if ($(location).attr('pathname') !== '/admin/shop/item/') {
    var allProps = ['manufacturer', 'type', 'price', 'power', 'minAmount', 'maxAmount'];
    var category = $("#id_category");
    var optionSelected = category.find("option:selected");
    $.ajax({
      type: 'GET',
      url: 'admin-ajax',
      dataType: 'json',
      success: function (json) {
        var data = json.data;
        if (!category.find('option')[0].selected) {
          checkProps(optionSelected, data, allProps, true)
        }
        category.change(function () {
          var optionSelected = $(this).find("option:selected");
          if (category.find('option')[0].selected) {
            slideChange(allProps, true, false)
          } else {
            checkProps(optionSelected, data, allProps, false)
          }
        });
      }
    });
  }
});


function checkProps(optionSelected, data, allProps, isNew) {
  for (var i = 0; i < data.length; i++) {
    if (optionSelected.attr('value') === data[i].id) {
      var thisProps = data[i].props;
      var notProps = $(allProps).not(thisProps).get();
      slideChange(thisProps, false, isNew);
      slideChange(notProps, true, isNew);
      break;
    }
  }
}


function slideChange(propList, isUp, isNew) {
  for (var i = 0; i < propList.length; i++) {
    var thisProp = $('.field-' + propList[i]);
    if (isUp) {
      if (isNew) {
        thisProp.hide();
      } else {
        thisProp.slideUp();
      }
    } else {
      if (isNew) {
        thisProp.show();
      } else {
        thisProp.slideDown();
      }
    }
  }
}