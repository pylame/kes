from django.contrib import admin
from .models import Category, Item
from django.http import JsonResponse


def view(request):
    data = []
    categories = Category.objects.all()

    for category in categories:
        propList = []
        if category.propManufacturer:
            propList.append('manufacturer')
        if category.propType:
            propList.append('type')
        if category.propPrice:
            propList.append('price')
        if category.propPower:
            propList.append('power')
        if category.propAmount:
            propList.append('minAmount')
            propList.append('maxAmount')

        data.append({
            'id': str(category.id),
            'props': propList
        })

    return JsonResponse({'data': data})


class ItemAdmin(admin.ModelAdmin):
    search_fields = ('name',)

    list_filter = ('category', 'manufacturer', 'type',)

    class Media:
        css = {
            'all': ('static/css/admin.css',)
        }
        js = (
            'static/js/jquery.js',
            'static/js/admin.js',
        )

    def get_urls(self):
        from django.conf.urls import url
        urls = super(ItemAdmin, self).get_urls()
        urls += [
            url(r'admin-ajax', self.admin_site.admin_view(view))
        ]
        return urls


admin.site.register(Category)
admin.site.register(Item, ItemAdmin)
