from django.apps import AppConfig

class ShopAppConfig(AppConfig):
    name = "shop"
    verbose_name = "Интернет-магазин"
