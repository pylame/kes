from django.conf.urls import url, include
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='shop/shop.html'), name='home'),
    url(r'^(?P<slug>[-*\w]+)/', include([
        url(r'^$', views.categoryPage),
        url(r'^search=', views.searchItems),
        url(r'^sort=', views.sortItems),
        url(r'^(?P<slug>[-*\w]+)/$', views.itemPage),
    ])),
]
