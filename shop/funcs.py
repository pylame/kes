from django.core.paginator import Paginator, EmptyPage


def getItems(sortItems, page):
    sortItems = sortItems.order_by('price', 'pk')
    paginator = Paginator(sortItems, 12)
    if sortItems.count() == 0:
        pages = 0
    else:
        pages = paginator.num_pages
    try:
        sortItems = paginator.page(page)
    except EmptyPage:
        sortItems = paginator.page(1)
    hasNext = sortItems.has_next()

    items = makeJSON(sortItems)

    data = {
        'pages': pages,
        'hasNext': hasNext,
        'items': items
    }

    return data


def makeJSON(sortItems):
    items = []

    for i in range(len(sortItems)):

        items.append({
            'name': sortItems[i].name,
            'slug': sortItems[i].url,
        })

        if hasattr(sortItems[i], 'power'):
            items[i].update({
                'power': sortItems[i].power
            })

        if hasattr(sortItems[i], 'minAmount') and hasattr(sortItems[i], 'maxAmount'):
            items[i].update({
                'minAmount': sortItems[i].minAmount,
                'maxAmount': sortItems[i].maxAmount
            })

        if hasattr(sortItems[i], 'price'):
            items[i].update({
                'price': sortItems[i].price
            })

    return items