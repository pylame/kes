from itertools import chain
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from .models import Category, Item, CHOICES_TYPE as categoriesModelT
from .funcs import getItems



def categoryPage(request, slug):
    categoryModel = get_object_or_404(Category, slug=slug)
    categoryName = categoryModel.name

    for categoryModelT in categoriesModelT:
        if categoryModelT[0] == categoryName:
            typesModel = categoryModelT[1]
            break
    else:
        typesModel = None

    return render(request, 'shop/categoryShop.html', {'categoryModel': categoryModel,
                                                      'typesModel': typesModel})


def searchItems(request, slug):
    if request.method == 'GET':
        search = request.GET.get('search')
        page = request.GET.get('page')

        if search == '':
            sortItems = Item.objects.filter(category__slug=slug)

        else:
            sortItems = Item.objects.filter(category__slug=slug).filter(name__icontains=search)

        data = getItems(sortItems, page)

        return JsonResponse(data)


def sortItems(request, slug):
    if request.method == 'GET':

        sortItems = Item.objects.filter(category__slug=slug)

        if request.GET.getlist('manufacturer'):
            manufacturers = request.GET.getlist('manufacturer')
            sortItems = sortItems.filter(manufacturer__in=manufacturers)

        if request.GET.getlist('type'):
            types = request.GET.getlist('type')
            sortItems = sortItems.filter(type__in=types)

        if request.GET.get('minPrice'):
            minPrice = request.GET.get('minPrice')
            sortItems = sortItems.filter(price__gte=minPrice)

        if request.GET.get('maxPrice'):
            maxPrice = request.GET.get('maxPrice')
            sortItems = sortItems.filter(price__lte=maxPrice)

        if request.GET.get('minPower'):
            minPower = request.GET.get('minPower')
            sortItems = sortItems.filter(power__gte=minPower)

        if request.GET.get('maxPower'):
            maxPower = request.GET.get('maxPower')
            sortItems = sortItems.filter(power__lte=maxPower)

        if request.GET.get('amount'):
            amount = request.GET.get('amount')
            sortItems = sortItems.filter(beforeAmount__lte=amount).filter(afterAmount__gte=amount)

        page = request.GET.get('page')

        data = getItems(sortItems, page)

        return JsonResponse(data)



def itemPage(request, slug):
    itemModel = get_object_or_404(Item, slug=slug)
    ignoreItems = Item.objects.exclude(slug=itemModel.slug)

    if hasattr(itemModel, 'type'):
        ignoreItems = ignoreItems.filter(type=itemModel.type)

    cognateItems = ignoreItems.order_by('price', 'pk').filter(price__gte=itemModel.price)[:3]
    if cognateItems.count() != 3:
        for i in range(3):
            j = 3 - i
            if cognateItems.count() == i:
                beforeItems = ignoreItems.order_by('-price', 'pk').filter(price__lt=itemModel.price)[:j]
                cognateItems = list(chain(cognateItems, beforeItems))
                break

    return render(request, 'shop/item.html', {'item': itemModel,
                                              'cognateItems': cognateItems})
