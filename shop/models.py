from django.db import models


class Category(models.Model):
    name = models.CharField(blank=True, null=True, max_length=100, verbose_name='Название')
    slug = models.SlugField(blank=True, null=True, verbose_name='Название в url')
    url = models.CharField(blank=True, null=True, max_length=100, editable=False)
    descr = models.TextField(blank=True, null=True, max_length=5000, verbose_name='Небольшое описание')
    title = models.CharField(blank=True, null=True, max_length=100, verbose_name='Тег title')
    text = models.TextField(blank=True, null=True, max_length=5000, verbose_name='Текст страницы')

    propPrice = models.BooleanField(default=False, verbose_name='Свойство - цена')
    propManufacturer = models.BooleanField(default=False, verbose_name='Свойство - производитель')
    propType = models.BooleanField(default=False, verbose_name='Свойство - тип')
    propPower = models.BooleanField(default=False, verbose_name='Свойство - мощность (кВт)')
    propAmount = models.BooleanField(default=False, verbose_name='Свойство - объем парильни')

    def save(self, *args, **kwargs):
        self.url = '/shop/' + self.slug
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория интернет-магазина'
        verbose_name_plural = 'Категории интернет-магазина'


# -------------------------------------------------CHOICES---------------------------------------------------------------

CHOICES_TYPE = (
    ('Печи', (
        ('Wood', 'Дровяная'),
        ('Electrical', 'Электрическая')
    )),
    ('Двери', (
        ('Sauna', 'Для сауны и бани'),
        ('Hamam', 'Для хамама и моечной'),
        ('Interior', 'Межкомнатные')
    ))
)

CHOICES_MANUFACTURER = (
        ('Harvia', 'Harvia'),
        ('Tylo', 'Tylo')
    )

# -----------------------------------------------END_CHOICES-------------------------------------------------------------


class Item(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    name = models.CharField(blank=True, null=True, max_length=100, verbose_name='Название')
    slug = models.SlugField(blank=True, null=True, editable=False)
    url = models.CharField(blank=True, null=True, max_length=100, editable=False)
    manufacturer = models.CharField(max_length=100, choices=CHOICES_MANUFACTURER, blank=True, null=True,
                                    verbose_name='Производитель')
    type = models.CharField(blank=True, null=True, max_length=100, choices=CHOICES_TYPE, verbose_name='Тип')
    price = models.PositiveIntegerField(blank=True, null=True, verbose_name='Цена')
    power = models.FloatField(blank=True, null=True, verbose_name='Мощность')
    minAmount = models.FloatField(blank=True, null=True, verbose_name='мин. объем парильни')
    maxAmount = models.FloatField(blank=True, null=True, verbose_name='макс. объем парильни')
    description = models.TextField(blank=True, null=True, verbose_name='описание')
    image = models.ImageField(upload_to='item', blank=True, null=True, verbose_name='картинка')

    def save(self, *args, **kwargs):
        super(Item, self).save(*args, **kwargs)
        self.slug = self.name.replace(' ', '_').lower() + '-' + str(self.id)
        self.url = '/shop/' + self.category.slug + '/' + self.slug
        super(Item, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
