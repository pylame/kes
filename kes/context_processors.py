from building.models import itemBuilding
from shop.models import Category


def get_cart_items(request):
    # del request.session['cart']
    # del request.session['cartInfo']

    sessionKey = request.session.session_key
    if not sessionKey:
        request.session.cycle_key()

    try:
        itemsInCart = request.session['cart']
    except KeyError:
        request.session['cart'] = []

    try:
        totalItemsInCart = request.session['cartInfo']['totalItems']
        totalPriceInCart = request.session['cartInfo']['totalPrice']
    except KeyError:
        request.session['cartInfo'] = {
            'totalItems': 0,
            'totalPrice': 0
        }

    return locals()


def get_building_items(request):
    itemsBuilding = itemBuilding.objects.all()

    return locals()


def get_categories_shop(request):
    itemsCategory = Category.objects.all()

    return locals()
