from django.http import JsonResponse, HttpResponse
from django.conf import settings
from django.core.mail import send_mail
from shop.models import Item

def addCart(request):
    url = request.GET.get('url')

    thisItem = Item.objects.get(url=url)
    name = thisItem.name
    price = thisItem.price

    if len(request.session['cart']) != 0:

        for i in range(len(request.session['cart'])):
            if request.session['cart'][i]['url'] == url:
                return HttpResponse('item_is_available')

        else:
            request.session['cart'].append({
                'name': name,
                'url': url,
                'price': price,
                'nmb': 1
            })

            totalPrice = request.session['cartInfo']['totalPrice']
            totalItems = request.session['cartInfo']['totalItems']

            request.session['cartInfo'].update({
                'totalPrice': totalPrice + price,
                'totalItems': totalItems + 1
            })


    else:
        request.session['cart'] = [{
            'name': name,
            'url': url,
            'price': price,
            'nmb': 1
        }]

        request.session['cartInfo'] = {
            'totalPrice': price,
            'totalItems': 1
        }

    cartItems = {
        'totalItems': request.session['cartInfo']['totalItems'],
        'totalPrice': request.session['cartInfo']['totalPrice'],
        'cart': request.session['cart']
    }

    return JsonResponse(cartItems)


def removeCart(request):
    url = request.GET.get('url')
    print(url)

    thisItem = Item.objects.get(url=url)
    price = thisItem.price

    for i in range(len(request.session['cart'])):
        if request.session['cart'][i]['url'] == url:

            totalPrice = request.session['cartInfo']['totalPrice']
            totalItems = request.session['cartInfo']['totalItems']
            nmb = request.session['cart'][i]['nmb']

            request.session['cartInfo'].update({
                'totalPrice': totalPrice - price * nmb,
                'totalItems': totalItems - 1
            })

            del request.session['cart'][i]

            cartItems = {
                'totalItems': request.session['cartInfo']['totalItems'],
                'totalPrice': request.session['cartInfo']['totalPrice'],
                'cart': request.session['cart']
            }

            return JsonResponse(cartItems)

    return HttpResponse('not_found', content_type='text/html')


def send_email(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        tel = request.POST.get('tel')
        email = request.POST.get('email')
        message = request.POST.get('message')

        if (name == '') or (len(name) < 2):
            return HttpResponse('name_error', content_type='text/html')

        try:
            if not '.' in email.split('@')[1]:
                return HttpResponse('email_error', content_type='text/html')

        except:
            return HttpResponse('email_error', content_type='text/html')

        br = '\n'
        data = name + br + tel + br + email + br + message

        subject = 'Форма обратной связи'

        send_mail(subject, data, settings.EMAIL_HOST_USER, ['qreditry@gmail.com'], fail_silently=False)

        return HttpResponse('ok', content_type='text/html')
